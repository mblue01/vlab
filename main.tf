# DEFINE PROVIDER
resource "random_pet" "prefix" {}

provider "aws" {
  region     = "us-east-2"
  access_key = var.aws_accessKey
  secret_key = var.aws_secretKey
} 

# Create Security Group - Allow Inbound Ports / All Outbound
resource "aws_security_group" "ssh-secgrp" {
    name = "EC2-SG-${random_pet.prefix.id}"
    ingress {
        protocol = "tcp"
        from_port = 22
        to_port = 22
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        protocol = "tcp"
        from_port = 80
        to_port = 80
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        protocol = "tcp"
        from_port = 443
        to_port = 443
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

# Create our EC2 Instances
resource "aws_instance" "ec2-ubuntu20_04" {
    ami                                  = "ami-0fb653ca2d3203ac1"                           #ubuntu 20.04
    count                                = var.instance_count
    instance_type                        = var.ec2_instance_type 
    key_name                             = var.ec2_keypair                                
    associate_public_ip_address          = true
   
    user_data = <<-EOF
	#!/bin/bash
	set -x
	exec > >(tee /var/log/user-data.log|logger -t user-data ) 2>&1
	echo BEGIN
	date '+%Y-%m-%d %H:%M:%S'
    hostnamectl set-hostname "p-app0${count.index}.bluetls.net"
    apt-get update
    apt-get install -y nginx git software-properties-common certbot python3-certbot-nginx
	hostname > /etc/hostname
    export MYHN=$(cat /etc/hostname)
    systemctl start nginx
    systemctl enable nginx
    mkdir -p /var/www/bluetls.com/html
    # -----run certbot
    certbot run -n --nginx --agree-tos -d $MYHN -m mike.blue01@gmail.com --redirect
	EOF

    tags = {
        Name = "p-app-${random_pet.prefix.id}-${count.index + 1}"
    }
    vpc_security_group_ids = [aws_security_group.ssh-secgrp.id]
   
}

resource "aws_route53_record" "tls_record" {
  count           = length(aws_instance.ec2-ubuntu20_04)            # get a count of aws ec2 instances
  allow_overwrite = true
  zone_id          = var.aws_zoneID                                 # Your AWS hardcoded Route 53 ID in tfvars file
  #name            = "p-app0${count.index}.bluetls.net"               
  name            = "p-app0${count.index}.${var.myDomain}"               
  type            = "A"
  ttl             = "300"
  records = [aws_instance.ec2-ubuntu20_04[count.index].public_ip]
}


output "Ubuntu_IP" {
    value = aws_instance.ec2-ubuntu20_04.*.public_ip
    
}
