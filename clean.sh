#!/usr/bin/bash
# CAUTION! This script will remove all Terraform Init/Apply artifacts.
# It will also remove all state files, so be careful!
rm -rf ./.terraform
rm -f ./terraform.tfstate
rm -f ./terraform.tfstate.backup
rm -f ./.terraform.lock.hcl