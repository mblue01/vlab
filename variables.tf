variable "ec2_keypair" {}
variable "ec2_instance_type" {}
variable "aws_accessKey" {}                 # AWS access key (IAM)
variable "aws_secretKey" {}                 # AWS secret key (IAM)
variable "instance_count" {}
variable "aws_zoneID" {}
variable "myDomain" {}