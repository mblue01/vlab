# terraform - vLab Demo Environment
 Create AWS EC2 environment for demo & testing purposes. This terraform recipe will spin up multiple EC2 instances, install NGINX, create a DNS A-record in AWS Route 53, and run certbot to install a certificate.
  *  NGINX will run https on standard port 443
  *  SSH (port 22) will require an AWS EC2 SSH key for connection
  *  'terraform destroy' will remove the DNS A-records, but will not remove your DNS zone, SOA, NS or previously created records
 
PreReqs:
- Terraform
- AWS Access & Secret Key
- Domain registered in AWS Route 53

AWS Setup:
 1. You will need to create (or utilize) your Access Key ID and Secret Key from your AWS user
-- Go to IAM, select your user
-- Go to "Security Credentials" and "Create Access Key"
   -- You will need both the access key and secret key.  Save these in a secure location! You will need these to run terraform!
 
 2. You will need an SSH key created in the region that matches your configuration (East-2)
 -- Navigate to "EC2" and switch to the region (East-2)
 -- On the left navigation pane, go to "Key Pairs"
 -- Click "Create Key Pair" (only need to do this once)
 -- Create key pair. Use a name that is identifiable to your project. Remember this name, it will go in your tfvars file
    -- Select RSA, .pem
    -- Click "Create key pair"
    -- Download the *.pem file to a secure location, as this is the private key that will be used to SSH into EC2 instances


 Terraform Instructions:

 1. Install Terraform on your desktop: https://www.terraform.io/downloads

 2. Copy the files in this git to a directory on your machine

 3. Set variables in terraform.tfvars
    *  Add your ssh key name exactly as you created it in AWS. If you forgot the name, you can look it up in "EC2" -> "Key Pairs"
    *  Select a count - this is how many EC2 instances will spin up
    *  Set your Zone ID. This ID can be found in your AWS Route 53 dashboard

 4. Open a terminal window and change into the directory you copied the files into

 5. Run:  `terraform init`
 -- This will download the AWS provider from hashicorp

 6. Run:  `terraform plan`
 -- You will need to paste in your access key THEN your secret key. Alternatively, you can hard-code them in the terraform.tfvars file

 7. Ensure there are no errors from the previous command.

 8. Run: `terraform apply`
 
    *  You will be asked to type in "yes" to continue. Once you enter "yes", terraform will spin up your environment in AWS.

 9. You can SSH to your EC2 box using it's public IP

    `ssh -i mykey.pem ubuntu@publicIP`

 10. Connect to your servers over https and ensure certificates have been installed properly
