# MUST SET ALL THESE
ec2_instance_type           = "t2.micro"
ec2_keypair                 = "Your SSH KeyName"                # key from AWS ec2 - Key is specific for it's region
instance_count              = 2                                 # Number of EC2 instances to spin up 
aws_zoneID                  = "Your Zone ID"                    # The Zone ID from your Amazon Route 53 DNS Manager.
myDomain                    = "mydomain.com"                    # Your registered domain in Route 53

# For dev & troubleshooting; You can hard-code these so you don't have to keep entering them on terraform apply
# !!! Do not upload these to the cloud or any git/storage accounts !!!
#aws_accessKey              = "Your AWS Access Key"
#aws_secretKey              = "Your AWS Secret Key"
